#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTimer>
#include <stdlib.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    showFullScreen();
    QTimer::singleShot(1000, this, SLOT(start()));

}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::start()
{
    system("/home/pi/RPiPLC/Aktualizuj.sh");
}
